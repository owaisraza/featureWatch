package com.dawateislami.featurewatch.notification;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.media.tv.TvInputManager;
import android.os.Build;
import android.util.Log;

import com.dawateislami.featurewatch.utils.Constants;

import static android.content.Context.JOB_SCHEDULER_SERVICE;

public class WatchSchedulerManager {

   private static final String  TAG = "WatchSchedulerManager";
   private Context mContext;
   private long periodicTime;
   public WatchSchedulerManager(Context mContext){
        this.mContext = mContext;
   }

   public void createJob(Class<?> service){
       JobScheduler  jobScheduler = (JobScheduler) mContext.getSystemService(JOB_SCHEDULER_SERVICE);
       ComponentName componentName = new ComponentName(mContext, service);
       JobInfo.Builder builder = new JobInfo.Builder(Constants.JOB_WATCH_ID,componentName);

       long repeatingTime = getPeriodicTime() != 0 ? getPeriodicTime() : Constants.DEFAULT_TIME;
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
           builder.setPeriodic(repeatingTime);
       else
           builder.setPeriodic(repeatingTime);

       builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
       builder.setPersisted(true);
       JobInfo jobInfo = builder.build();

       assert jobScheduler != null;
       int jobStatus = jobScheduler.schedule(jobInfo);

       if(jobStatus != JobScheduler.RESULT_SUCCESS)
           Log.d(TAG,"Job not initialize");
       else
           Log.d(TAG,"Job initialize");
   }


    private long getPeriodicTime() {
        return periodicTime;
    }

    public void setPeriodicTime(long periodicTime) {
        this.periodicTime = periodicTime;
    }
}
