package com.dawateislami.featurewatch;

import android.app.Activity;

import com.dawateislami.featurewatch.beans.ActivityInfo;
import com.dawateislami.featurewatch.beans.ApplicationInfo;
import com.dawateislami.featurewatch.database.DBManager;

import java.util.List;

public class WatchFeature {

    private Activity activity;
    private ApplicationInfo appInfo;
    private ActivityInfo activityInfo;
    private List<ActivityInfo> activityName;
    private static WatchFeature handler;
    private DBManager db;

    private WatchFeature(Activity activity) {
        this.activity = activity;
    }

    public static WatchFeature getInstance(Activity activity) {
        if(handler == null)
            synchronized(WatchFeature.class) {
                if(handler == null){
                    handler = new WatchFeature(activity);
                }
            }
        return handler;
    }

    public void init(){
        db = DBManager.getInstance(activity.getApplicationContext());
        ApplicationInfo applicationInfo = getAppInfo();
        isApplicationRegister(applicationInfo);
        List<ActivityInfo> acNames = getActivityName();
        if(acNames != null)
            isActivityRegister(acNames);

    }

    public void build(){

    }

    private void isApplicationRegister(ApplicationInfo applicationInfo){
        if(applicationInfo != null){
            String appId = db.getApplicationInfo(applicationInfo.getAppId()).getAppId();
            if(appId != null) {
                if (db.getApplicationInfo(applicationInfo.getAppId()).getAppId().equals(applicationInfo.getAppId())) {
                    db.updateApplicationInfo(applicationInfo);
                } else {
                    db.addApplicationInfo(applicationInfo);
                }
            }else{
                db.addApplicationInfo(applicationInfo);
            }
        }
    }

    private void isActivityRegister(List<ActivityInfo> activityName){
        for(ActivityInfo info : activityName){
            if(info.getNameActivity() != null){
                db.addActivityInfo(info);
            }
        }
    }

    private ApplicationInfo getAppInfo(){
        return this.appInfo;
    }

    public WatchFeature applicationInfo(ApplicationInfo appInfo){
        this.appInfo = appInfo;
        return this;
    }

    private ActivityInfo getActivityInfo(){
        return this.activityInfo;
    }

    public WatchFeature activityInfo(ActivityInfo activityInfo){
        this.activityInfo = activityInfo;
        return this;
    }

    private List<ActivityInfo> getActivityName() {
        return activityName;
    }

    public void setActivityName(List<ActivityInfo> activityName) {
        this.activityName = activityName;
    }
}
