package com.dawateislami.featurewatch.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dawateislami.featurewatch.WatchFeature;
import com.dawateislami.featurewatch.beans.ActivityInfo;
import com.dawateislami.featurewatch.beans.ApplicationInfo;
import com.dawateislami.featurewatch.beans.NotificationInfo;
import com.dawateislami.featurewatch.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class DBManager extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "featureWatcherDB";

    private String TABLE_APPLICATION = "application_info";
    private String TABLE_ACTIVITY = "activity_info";
    private String TABLE_NOTIFICATION = "notification_info";


    private String KEY_APPLICATION_ID = "app_id";
    private String KEY_APPLICATION_NAME = "app_name";
    private String KEY_APPLICATION_CODE = "app_code";
    private String KEY_APPLICATION_VERSION = "app_version";
    private String KEY_APPLICATION_CREATED_DATE = "created_date";
    private String KEY_APPLICATION_MODIFIED_DATE = "modified_date";
    private String KEY_APPLICATION_ENABLE = "enable";

    private String [] COLOUMN_APPLICATION = {KEY_APPLICATION_ID,KEY_APPLICATION_NAME,KEY_APPLICATION_CODE,
            KEY_APPLICATION_VERSION,KEY_APPLICATION_CREATED_DATE,KEY_APPLICATION_MODIFIED_DATE,KEY_APPLICATION_ENABLE};

    private String KEY_ACTIVITY_ID = "id";
    private String KEY_ACTIVITY_NAME = "activity_name";
    private String KEY_ACTIVITY_CLASS = "activity_class_name";
    private String KEY_ACTIVITY_APP_ID = "app_id";
    private String KEY_ACTIVITY_PRIORITY = "priority";
    private String KEY_ACTIVITY_CREATED_DATE = "created_date";
    private String KEY_ACTIVITY_MODIFIED_DATE = "modified_date";
    private String KEY_ACTIVITY_ENABLE = "enable";

    private String [] COLOUMN_ACTIVITY = {KEY_ACTIVITY_ID,KEY_ACTIVITY_NAME,KEY_ACTIVITY_CLASS,
            KEY_ACTIVITY_APP_ID,KEY_ACTIVITY_PRIORITY,KEY_ACTIVITY_CREATED_DATE,
            KEY_ACTIVITY_MODIFIED_DATE,KEY_ACTIVITY_ENABLE};


    private String KEY_NOTIFICATION_ID = "id";
    private String KEY_NOTIFICATION_ACTIVITY_ID = "activity_id";
    private String KEY_NOTIFICATION_COUNT = "notification_count";
    private String KEY_NOTIFICATION_IS_DELIVERED = "is_delivered";
    private String KEY_NOTIFICATION_IS_PENDING = "is_pending";
    private String KEY_NOTIFICATION_IS_CANCEL = "is_cancel";
    private String KEY_NOTIFICATION_CREATED_DATE = "created_date";
    private String KEY_NOTIFICATION_MODIFIED_DATE = "modified_date";

    private String [] COLOUMN_NOTIFICATION = {KEY_NOTIFICATION_ID,KEY_NOTIFICATION_ACTIVITY_ID,
            KEY_NOTIFICATION_COUNT,KEY_NOTIFICATION_IS_DELIVERED,KEY_NOTIFICATION_IS_PENDING,
            KEY_NOTIFICATION_IS_CANCEL,KEY_NOTIFICATION_CREATED_DATE,KEY_NOTIFICATION_MODIFIED_DATE};


    private static DBManager handler;

    public static DBManager getInstance(Context context) {
        if(handler == null)
            synchronized(DBManager.class) {
                if(handler == null){
                    handler = new DBManager(context);
                }
            }
        return handler;
    }

    private String CREATE_TABLE_APPLICATION = " CREATE TABLE IF NOT EXISTS  " + TABLE_APPLICATION + "(" +
            KEY_APPLICATION_ID + " TEXT NOT NULL,"+
            KEY_APPLICATION_NAME + " TEXT ," +
            KEY_APPLICATION_CODE + " TEXT ," +
            KEY_APPLICATION_VERSION + " INTEGER ," +
            KEY_APPLICATION_ENABLE + " INTEGER ," +
            KEY_APPLICATION_CREATED_DATE + " INTEGER ," +
            KEY_APPLICATION_MODIFIED_DATE + " INTEGER )";

    private String CREATE_TABLE_ACTIVITY = " CREATE TABLE IF NOT EXISTS  " + TABLE_ACTIVITY + "(" +
            KEY_ACTIVITY_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
            KEY_ACTIVITY_NAME + " TEXT ," +
            KEY_ACTIVITY_CLASS + " TEXT ," +
            KEY_ACTIVITY_APP_ID + " TEXT ," +
            KEY_ACTIVITY_PRIORITY + " INTEGER ," +
            KEY_ACTIVITY_ENABLE + " INTEGER ," +
            KEY_ACTIVITY_CREATED_DATE + " INTEGER ," +
            KEY_ACTIVITY_MODIFIED_DATE + " INTEGER )";

    private String CREATE_TABLE_NOTIFICATION = " CREATE TABLE IF NOT EXISTS  " + TABLE_NOTIFICATION + "(" +
            KEY_NOTIFICATION_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
            KEY_NOTIFICATION_ACTIVITY_ID + " INTEGER ," +
            KEY_NOTIFICATION_COUNT + " INTEGER ," +
            KEY_NOTIFICATION_IS_DELIVERED + " INTEGER ," +
            KEY_NOTIFICATION_IS_PENDING + " INTEGER ," +
            KEY_NOTIFICATION_IS_CANCEL + " INTEGER ," +
            KEY_NOTIFICATION_CREATED_DATE + " INTEGER ," +
            KEY_NOTIFICATION_MODIFIED_DATE + " INTEGER )";

    private DBManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_APPLICATION);
        db.execSQL(CREATE_TABLE_ACTIVITY);
        db.execSQL(CREATE_TABLE_NOTIFICATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long addApplicationInfo(ApplicationInfo info){
        SQLiteDatabase database = getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_APPLICATION_NAME,info.getAppName());
        values.put(KEY_APPLICATION_CODE,info.getAppCode());
        values.put(KEY_APPLICATION_VERSION,info.getAppVersion());
        values.put(KEY_APPLICATION_ENABLE,info.getEnable());
        values.put(KEY_APPLICATION_ID,info.getAppId());
        values.put(KEY_APPLICATION_CREATED_DATE,info.getCreatedDate());
        values.put(KEY_APPLICATION_MODIFIED_DATE,info.getCreatedDate());
        long row = database.insert(TABLE_APPLICATION,null,values);
        database.close();
        return row;
    }

   public ApplicationInfo getApplicationInfo(String appId){
       SQLiteDatabase database = getReadableDatabase();
       ApplicationInfo applicationInfo = new ApplicationInfo();
       Cursor cursor = database.query(TABLE_APPLICATION,COLOUMN_APPLICATION,KEY_APPLICATION_ID + " = '"+appId+"'" ,null,null,null,null);
       if(cursor != null && !cursor.isClosed() && cursor.moveToFirst()){
            applicationInfo.setAppId(cursor.getString(cursor.getColumnIndex(KEY_APPLICATION_ID)));
            applicationInfo.setAppName(cursor.getString(cursor.getColumnIndex(KEY_APPLICATION_NAME)));
            applicationInfo.setAppCode(cursor.getString(cursor.getColumnIndex(KEY_APPLICATION_CODE)));
            applicationInfo.setAppVersion(cursor.getInt(cursor.getColumnIndex(KEY_APPLICATION_VERSION)));
            applicationInfo.setCreatedDate(cursor.getInt(cursor.getColumnIndex(KEY_APPLICATION_CREATED_DATE)));
            applicationInfo.setModifiedDate(cursor.getInt(cursor.getColumnIndex(KEY_APPLICATION_MODIFIED_DATE)));
            applicationInfo.setEnable(cursor.getInt(cursor.getColumnIndex(KEY_APPLICATION_ENABLE)));
       }
       assert  cursor!= null;
       cursor.close();
       return  applicationInfo;
   }

   public long updateApplicationInfo(ApplicationInfo info){
       SQLiteDatabase database = getReadableDatabase();
       ContentValues values = new ContentValues();
       values.put(KEY_APPLICATION_NAME,info.getAppName());
       values.put(KEY_APPLICATION_CODE,info.getAppCode());
       values.put(KEY_APPLICATION_VERSION,info.getAppVersion());
       values.put(KEY_APPLICATION_ENABLE,info.getEnable());
       values.put(KEY_APPLICATION_MODIFIED_DATE,info.getModifiedDate());
       long row = database.update(TABLE_APPLICATION,values,KEY_APPLICATION_ID + " = '"+info.getAppId()+"'",null);
       database.close();
       return row;
   }


   public void addActivityInfo(ActivityInfo info){
       SQLiteDatabase database = getReadableDatabase();
       ContentValues values = new ContentValues();
       values.put(KEY_ACTIVITY_NAME,info.getNameActivity());
       values.put(KEY_ACTIVITY_CLASS,info.getActivityClassName());
       values.put(KEY_ACTIVITY_APP_ID,info.getApplicationId());
       values.put(KEY_ACTIVITY_PRIORITY,info.getPriority());
       values.put(KEY_ACTIVITY_ENABLE,info.getEnable());
       values.put(KEY_ACTIVITY_CREATED_DATE,info.getCreatedDate());
       values.put(KEY_ACTIVITY_MODIFIED_DATE,info.getCreatedDate());
       database.insert(TABLE_ACTIVITY,null,values);
       database.close();
   }

    public ActivityInfo getActivityInfo(String name){
        SQLiteDatabase database = getReadableDatabase();
        ActivityInfo info = new ActivityInfo();
        Cursor cursor = database.query(TABLE_ACTIVITY,COLOUMN_ACTIVITY,KEY_ACTIVITY_NAME + " = '"+name+"' ",null,null,null,null);
        if(cursor != null && !cursor.isClosed() && cursor.moveToFirst()){
            info.setId(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_ID)));
            info.setNameActivity(cursor.getString(cursor.getColumnIndex(KEY_ACTIVITY_NAME)));
            info.setActivityClassName(cursor.getString(cursor.getColumnIndex(KEY_ACTIVITY_CLASS)));
            info.setApplicationId(cursor.getString(cursor.getColumnIndex(KEY_ACTIVITY_APP_ID)));
            info.setPriority(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_PRIORITY)));
            info.setEnable(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_ENABLE)));
            info.setCreatedDate(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_CREATED_DATE)));
            info.setModifiedDate(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_MODIFIED_DATE)));
        }
        assert cursor != null;
        cursor.close();
        return info;
    }

    public List<ActivityInfo> getActivityInfo(){
        SQLiteDatabase database = getReadableDatabase();
        List<ActivityInfo> list = new ArrayList<>();
        Cursor cursor = database.query(TABLE_ACTIVITY,COLOUMN_ACTIVITY,null,null,null,null,KEY_ACTIVITY_PRIORITY + " ASC ");
        if(cursor != null && !cursor.isClosed() && cursor.moveToFirst()){
            do{
                ActivityInfo info = new ActivityInfo();
                info.setId(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_ID)));
                info.setNameActivity(cursor.getString(cursor.getColumnIndex(KEY_ACTIVITY_NAME)));
                info.setActivityClassName(cursor.getString(cursor.getColumnIndex(KEY_ACTIVITY_CLASS)));
                info.setApplicationId(cursor.getString(cursor.getColumnIndex(KEY_ACTIVITY_APP_ID)));
                info.setPriority(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_PRIORITY)));
                info.setEnable(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_ENABLE)));
                info.setCreatedDate(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_CREATED_DATE)));
                info.setModifiedDate(cursor.getInt(cursor.getColumnIndex(KEY_ACTIVITY_MODIFIED_DATE)));
                list.add(info);
            }while (cursor.moveToNext());
        }
        assert cursor != null;
        cursor.close();
        return list;
    }

    public long updateActivityInfo(ActivityInfo info){
        SQLiteDatabase database = getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ACTIVITY_PRIORITY,info.getPriority());
        values.put(KEY_ACTIVITY_ENABLE,info.getEnable());
        values.put(KEY_ACTIVITY_MODIFIED_DATE,info.getModifiedDate());
        long row = database.update(TABLE_ACTIVITY,values,KEY_ACTIVITY_APP_ID + " = '"
                + info.getApplicationId() + "' and  " + KEY_ACTIVITY_NAME + " = '" + info.getNameActivity() + "' ",null);
        database.close();
        return row;
    }

    public void addNoitification(NotificationInfo info){
        SQLiteDatabase database = getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NOTIFICATION_ACTIVITY_ID,info.getActivityId());
        values.put(KEY_NOTIFICATION_COUNT,info.getNotificationCount());
        values.put(KEY_NOTIFICATION_IS_DELIVERED,info.getIsDelivered());
        values.put(KEY_NOTIFICATION_IS_PENDING,info.getIsPending());
        values.put(KEY_NOTIFICATION_IS_CANCEL,info.getIsCancel());
        values.put(KEY_NOTIFICATION_CREATED_DATE,info.getCreatedDate());
        values.put(KEY_NOTIFICATION_MODIFIED_DATE,info.getCreatedDate());
        database.insert(TABLE_NOTIFICATION,null,values);
        database.close();
    }

    public long updateNoitification(NotificationInfo info){
        SQLiteDatabase database = getReadableDatabase();
        ContentValues values = new ContentValues();

        if(info.getNotificationCount() != 0 )
            values.put(KEY_NOTIFICATION_COUNT,info.getNotificationCount());
        if(info.getIsDelivered() != 0 )
            values.put(KEY_NOTIFICATION_IS_DELIVERED,info.getIsDelivered());
        if(info.getIsPending() != 0 )
            values.put(KEY_NOTIFICATION_IS_PENDING,info.getIsPending());
        if(info.getIsCancel() != 0 )
            values.put(KEY_NOTIFICATION_IS_CANCEL,info.getIsCancel());

        values.put(KEY_NOTIFICATION_MODIFIED_DATE,info.getModifiedDate());
        long row = database.update(TABLE_NOTIFICATION,values,KEY_NOTIFICATION_ACTIVITY_ID + " = " +info.getActivityId(),null);
        database.close();
        return row;
    }


}
