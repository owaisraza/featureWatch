package com.dawateislami.activityscheduleservice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.dawateislami.featurewatch.WatchFeature;
import com.dawateislami.featurewatch.beans.ActivityInfo;
import com.dawateislami.featurewatch.beans.ApplicationInfo;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WatchFeature watchFeature = WatchFeature.getInstance(MainActivity.this);
        watchFeature.setActivityName(getActivities());
        watchFeature.applicationInfo(applicationInfo());
        watchFeature.init();
    }

    private List<ActivityInfo> getActivities(){
        return null;
    }

    private ApplicationInfo applicationInfo(){
        return null;
    }
}
